citbx_use "multitarget"

citbx_module_os_setup() {
    OS_IMAGE_COMPRESS_DISABLED=true
    citbx_export OS_IMAGE_COMPRESS_DISABLED
}

citbx_module_os_before() {
    : ${OS_NAME:=$TARGET_NAME}
    : ${OS_VERSION:=$TARGET_VERSION}
    OS_IMAGE_DIR="$OS_NAME/$OS_VERSION"
    OS_IMAGE_PATH="$CI_PROJECT_DIR/artifacts/targets/$OS_IMAGE_DIR"
}
