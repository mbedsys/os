citbx_use "dind"
citbx_use "os"

job_define() {
    bashopts_declare -n TARGET_HOSTS -l target-host-list \
        -d "Target host list ('host1 host2 etc.')" -t string -r -s
    bashopts_declare -n BOOT_LOADER_TYPE -l boot-loader-type -o L \
        -d "Boot loader type" -t enumeration -e grub -e efi -v grub -s
    bashopts_declare -n OS_SSH_RSA_PRIVATE_KEY -l ssh-rsa-private-key \
        -d "SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n OS_SSH_DSA_PRIVATE_KEY -l ssh-dsa-private-key \
        -d "SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n OS_SSH_ECDSA_PRIVATE_KEY -l ssh-ecdsa-private-key \
        -d "SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n OS_SSH_ED25519_PRIVATE_KEY -l ssh-ed25519-private-key \
        -d "SSH private key to connect to the hosts" -t string -s
    bashopts_declare -n DOCKER_NETWORK -l network \
        -d "Docker network" -t enumeration $(
            printf " -e %s" $(docker network ls --format '{{.Name}}')
        ) -v bridge
    bashopts_declare -n NO_SEND_HASH_FILE -l no-send-hash-file \
        -d "Do not send hash file" -t boolean -s
    citbx_export TARGET_HOSTS BOOT_LOADER_TYPE NO_SEND_HASH_FILE OS_SSH_RSA_PRIVATE_KEY \
        OS_SSH_DSA_PRIVATE_KEY OS_SSH_ECDSA_PRIVATE_KEY OS_SSH_ED25519_PRIVATE_KEY
}

job_setup() {
    if [ "$CITBX_JOB_EXECUTOR" == "docker" ] \
        && [ -z "$OS_SSH_RSA_PRIVATE_KEY" ] \
        && [ -z "$OS_SSH_DSA_PRIVATE_KEY" ] \
        && [ -z "$OS_SSH_ECDSA_PRIVATE_KEY" ] \
        && [ -z "$OS_SSH_ED25519_PRIVATE_KEY" ]; then
        for type in rsa dsa ecdsa ed25519; do
            if [ -f $HOME/.ssh/id_$type ]; then
                citbx_docker_run_add_args -v "$HOME/.ssh/id_$type:$HOME/.ssh/id_$type" \
                    -v "$HOME/.ssh/id_$type:/root/.ssh/id_$type"
            fi
        done
    fi
    citbx_docker_run_add_args --net "$DOCKER_NETWORK"
}

job_main() {
    local target_host
    local target_user=${TARGET_USER:-root}
    if [ -z "$TARGET_HOSTS" ]; then
        print_critical "Undeclared TARGET_HOSTS. This environment variable must contains the host list"
    fi
    for type in rsa dsa ecdsa ed25519; do
        if eval "[ -n \"\$OS_SSH_${type^^}_PRIVATE_KEY\" ]"; then
            eval "echo -e \"\$OS_SSH_${type^^}_PRIVATE_KEY\" > $HOME/.ssh/id_$type"
            mkdir -p $HOME/.ssh
            sudo chown $(id -u):$(id -g) $HOME/.ssh
            chmod 700 $HOME/.ssh
            chmod 600 $HOME/.ssh/id_$type
        fi
    done
    if [ $(find $HOME/.ssh/ -regex '.*/id_\(rsa\|dsa\|ecdsa\|ed25519\)' | wc -l) -eq 0 ]; then
        print_warning "No SSH private key found into $HOME/.ssh" \
            "You may have to define one of OS_SSH_(RSA|DSA|ECDSA|ED25519)_PRIVATE_KEY environment variable"
    fi
    local ssh_opts=(
        -o StrictHostKeyChecking=accept-new -o LogLevel=ERROR -o UserKnownHostsFile=/dev/null
    )
    for target_host in $TARGET_HOSTS; do
        print_info "Deploying onto target $target_host..."
        ssh "${ssh_opts[@]}" $target_user@$target_host "rm -rf /media/data/upgrade/*"
        for f in $(awk '{print $2;}' "$OS_IMAGE_PATH"/upgrade-$BOOT_LOADER_TYPE.sha512); do
            scp "${ssh_opts[@]}" "$OS_IMAGE_PATH"/$f $target_user@$target_host:/media/data/upgrade/
        done
        if [ "$NO_SEND_HASH_FILE" != "true" ]; then
            scp "${ssh_opts[@]}" "$OS_IMAGE_PATH"/upgrade-$BOOT_LOADER_TYPE.sha512 \
                $target_user@$target_host:/media/data/upgrade/upgrade.sha512
        fi
    done
}

job_after() {
    local retcode=$1
    if [ $retcode -eq 0 ]; then
        print_info "Image successfully deployed"
    fi
}
