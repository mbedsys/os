citbx_use "dind"
citbx_use "runshell"
citbx_use "os"

target_build() {
    # Build docker image
    TARGET_DOCKER_BUILD_ARGS+=(-t "$TARGET_CI_REGISTRY_IMAGE_TAG")
    for build_arg in $DOCKER_BUILD_ARG_LIST; do
        TARGET_DOCKER_BUILD_ARGS+=(--build-arg "$build_arg"="${!build_arg}")
    done
    docker build "${TARGET_DOCKER_BUILD_ARGS[@]}" "targets/$TARGET_NAME/"
    if [ "$TARGET_LATEST_VERSION_ENABLED" == "true" ]; then
        docker tag "$TARGET_CI_REGISTRY_IMAGE_TAG" "${TARGET_CI_REGISTRY_IMAGE_TAG%:*}:latest"
    fi
    if [ -n "$CI_COMMIT_TAG" -o "$TARGET_DOCKER_PUSH_ENABLED" == "true" ] && [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push "$TARGET_CI_REGISTRY_IMAGE_TAG"
        if [ "$TARGET_LATEST_VERSION_ENABLED" == "true" ]; then
            docker push "${TARGET_CI_REGISTRY_IMAGE_TAG%:*}:latest"
        fi
    fi
}

target_mkimage() {
    sudo rm -rf build

    # Export docker filesystem
    if [ "$DOCKER_HOST" != "unix:///var/run/docker.sock" ]; then
        # Login only if runned in DIND mode (e.g. on a Gitlab runner and not on a workstation)
        docker login --username gitlab-ci-token --password-stdin "$CI_REGISTRY" <<< "$CI_JOB_TOKEN"
    fi
    OS_DOCKER_EXPORT_ID=$(docker run -d "$TARGET_CI_REGISTRY_IMAGE_TAG" sleep infinity)
    print_note "Extracting rootfs from $TARGET_CI_REGISTRY_IMAGE_TAG docker image..."
    mkdir -p build/rootfs
    docker export $OS_DOCKER_EXPORT_ID | sudo tar -C build/rootfs -x
    echo "${TARGET_HOSTNAME:-$OS_NAME}" | sudo tee build/rootfs/etc/hostname > /dev/null
    sudo truncate -s 0 build/rootfs/etc/resolv.conf

    # Extract vmlinuz & initramfs files
    mkdir -p $OS_IMAGE_PATH
    rm -rf $OS_IMAGE_PATH/*
    local vmlinuz_file=$(find build/rootfs/boot/ -type f -name "vmlinuz-*")
    if [ -z "$vmlinuz_file" ]; then
        print_critical "You must have one kernel image installed (e.g.: linux-lts) into the docker image"
    fi
    if [ "$(wc -l <<< "$vmlinuz_file")" -ne 1 ]; then
        print_critical "You cannot have more than one kernel image installed into the docker image"
    fi
    local kernel_version=${vmlinuz_file:26}
    if [ -f build/rootfs/etc/boot_part_label ]; then
        OS_BOOT_PART_LABEL="$(cat build/rootfs/etc/boot_part_label)"
        rm build/rootfs/etc/boot_part_label
    fi
    : ${OS_SYSTEM_TYPE:="file"}
    case "$OS_SYSTEM_TYPE" in
        file)
            : ${OS_BOOT_PART_LABEL:="SYSTEM"}
            : ${OS_SYSTEM_PART_LABEL:="SYSTEM"}
            : ${OS_BOOT_PART_SIZE:="$((4 * 1024 * 1024 * 1024))"}
            OS_ROOT="/media/${OS_BOOT_PART_LABEL,,}/rootfs.squashfs"
        ;;
        part)
            : ${OS_BOOT_PART_LABEL:="BOOT"}
            : ${OS_SYSTEM_PART_LABEL:="SYSTEM"}
            : ${OS_BOOT_PART_SIZE:="$((512 * 1024 * 1024))"}
            OS_ROOT="PARTLABEL=$OS_SYSTEM_PART_LABEL"
        ;;
    esac
    if [ "${OS_BOOT_GRUB:-"true"}" == "true" ]; then
        sudo cat build/rootfs/boot/efi/boot/grubx64.efi > "$OS_IMAGE_PATH/grubx64.efi"
        sudo sed "s|__OS_ROOT__|$OS_ROOT|" build/rootfs/boot/grub/grub.cfg > "$OS_IMAGE_PATH/grub.cfg"
    fi
    sudo cat build/rootfs/boot/vmlinuz-$kernel_version > build/vmlinuz
    sudo cat build/rootfs/boot/initramfs-$kernel_version > build/initramfs
    sudo rm build/rootfs/boot/{efi/boot/grubx64.efi,vmlinuz-*,initramfs-*}

    # Write image info
    printf '%s="%s"'"\n" OS_NAME $OS_NAME \
        OS_VERSION $OS_VERSION \
        TARGET_DOCKER_IMAGE_TAG $TARGET_CI_REGISTRY_IMAGE_TAG \
        | sudo sudo tee -a build/rootfs/etc/os-release > /dev/null

    # Build image files
    sudo mksquashfs build/rootfs build/rootfs.squashfs -no-recovery -noappend -comp xz -e boot
    sudo cat build/rootfs.squashfs > "$OS_IMAGE_PATH/rootfs.squashfs"
    if [ "${OS_IMAGE_LINUX:-"true"}" == "true" ]; then
        cp build/vmlinuz "$OS_IMAGE_PATH/vmlinuz"
    fi
    if [ "${OS_IMAGE_INITRD:-"true"}" == "true" ]; then
        cp build/initramfs "$OS_IMAGE_PATH/initramfs"
    fi
    if [ "${OS_BOOT_EFI:-"true"}" == "true" ]; then
        printf "%s" "modules=loop,squashfs root=$OS_ROOT" \
            " rootfstype=squashfs overlaytmpfs=yes intel_iommu=on amd_iommu=on" \
            " $OS_ADD_BOOT_ARGS" > build/cmdline
        objcopy --add-section .osrel="build/rootfs/etc/os-release" --change-section-vma .osrel=0x20000 \
                --add-section .cmdline="build/cmdline" --change-section-vma .cmdline=0x30000 \
                --add-section .linux="build/vmlinuz" --change-section-vma .linux=0x2000000 \
                --add-section .initrd="build/initramfs" --change-section-vma .initrd=0x3000000 \
                build/rootfs/usr/lib/gummiboot/linuxx64.efi.stub "$OS_IMAGE_PATH/linuxx64.efi"
    fi

    local system_folder=${OS_BOOT_PART_LABEL,,}
    if [ -d artifacts/${system_folder} ]; then
        mkdir -p build
        cp -a artifacts/${system_folder} build/${system_folder}
        readarray -t files <<<"$(find artifacts/${system_folder} -type f)"
        for f in "${files[@]}"; do
            cp "$f" "$OS_IMAGE_PATH/${f##*/}"
        done
    fi

    # Add system upgrade script
    if [ -f targets/$TARGET_NAME/sysupgrade-script.sh ]; then
        cp targets/$TARGET_NAME/sysupgrade-script.sh $OS_IMAGE_PATH/upgrade.sh
        local files=$(ls -1 $OS_IMAGE_PATH/)
        (
            cd $OS_IMAGE_PATH/ \
            && sha512sum grubx64.efi grub.cfg initramfs rootfs.squashfs upgrade.sh vmlinuz \
                > $OS_IMAGE_PATH/upgrade-grub.sha512 \
            && sha512sum linuxx64.efi rootfs.squashfs upgrade.sh > $OS_IMAGE_PATH/upgrade-efi.sha512 \
            && ln -s upgrade-grub.sha512 upgrade.sha512)
    fi
}

target_mksystempart() {
    local system_folder=${OS_BOOT_PART_LABEL,,}
    print_note "Collecting ${system_folder} files..."
    mkdir -p build/${system_folder}/efi/boot
    : ${OS_BOOT_LOADER:="efi"}
    if [ "$OS_BOOT_LOADER" == "efi" ]; then
        cp "$OS_IMAGE_PATH/linuxx64.efi" build/${system_folder}/efi/boot/bootx64.efi
    else
        cp "$OS_IMAGE_PATH/grubx64.efi" build/${system_folder}/efi/boot/bootx64.efi
        mkdir -p build/${system_folder}/boot/grub
        cp "$OS_IMAGE_PATH/grub.cfg" build/${system_folder}/boot/grub/
        cp "$OS_IMAGE_PATH"/{vmlinuz,initramfs} build/${system_folder}/boot/
    fi
    if [ "$OS_SYSTEM_TYPE" == "file" ]; then
        cp "$OS_IMAGE_PATH/rootfs.squashfs" build/${system_folder}/rootfs.squashfs
    fi
    mkdir -p build/${system_folder}/start.d
    echo "passwd -d root" >> build/${system_folder}/start.d/50-remove-root-passwd.sh

    local system_part_sise="$OS_BOOT_PART_SIZE"
    print_note "Building the ${system_folder} partition image (${system_part_sise} Bytes)..."
    truncate -s "${system_part_sise}" build/boot-part.img
    mkfs.vfat -F 32 -n $OS_BOOT_PART_LABEL build/boot-part.img
    mcopy -bsp -i build/boot-part.img build/${system_folder}/* ::

    if [ "$OS_IMAGE_COMPRESS_DISABLED" == "true" ]; then
        ln build/boot-part.img "$OS_IMAGE_PATH/boot-part.img"
    else
        print_note "Compressing the ${system_folder} partition image..."
        cat build/boot-part.img | xz -T0 -c9 > "$OS_IMAGE_PATH/boot-part.img.xz"
    fi
}

target_mksystemdisk() {
    print_note "Building the disk image..."
    local build_disk_image=build/disk.img
    dd bs=512k count=2 of=${build_disk_image} if=/dev/zero status=none
    cat build/boot-part.img >> ${build_disk_image}
    local disk_sise="${OS_DISK_SIZE:-16G}"
    truncate -s $disk_sise ${build_disk_image}
    parted --script --align=optimal ${build_disk_image} \
        mklabel gpt \
        mkpart ${OS_BOOT_PART_LABEL} fat32 1MiB "$(($(
            stat -c %s build/boot-part.img
        ) + 1024*1024))B" \
        set 1 boot on

    if [ "$OS_IMAGE_COMPRESS_DISABLED" == "true" ]; then
        ln ${build_disk_image} "$OS_IMAGE_PATH/disk.img"
    else
        print_note "Compressing the system disk image..."
        cat ${build_disk_image} | xz -T0 -c9 > "$OS_IMAGE_PATH/disk.img.xz"
    fi
}

job_define() {
    bashopts_check_numfmt() {
        if ! numfmt --from auto "$1"; then
            bashopts_log E "Option $op: '$1' is not a valid size value"
            return 1
        fi
        return 0
    }
    bashopts_declare -n OS_BOOT_PART_SIZE -l boot-part-size \
        -d "Custom image size in bytes" -t string -k bashopts_check_numfmt
    bashopts_declare -n OS_BOOT_PART_BUILD_ENABLED -l boot-part-image \
        -d "Build boot partition image" -t boolean
    bashopts_declare -n OS_DISK_BUILD_ENABLED -l disk-image \
        -d "Build disk image" -t boolean
    bashopts_declare -n OS_SYSTEM_TYPE -l system-type \
        -d "System type" -t enumeration -e file -e part -v part
    bashopts_declare -n OS_BOOT_LOADER -l boot-loader \
        -d "System type" -t enumeration -e grub -e efi -v efi
    bashopts_declare -n OS_ADD_BOOT_ARGS -l add-boot-args \
        -d "Add boot arguments" -t string
    citbx_export    OS_BOOT_PART_BUILD_ENABLED OS_DISK_BUILD_ENABLED \
                    OS_SYSTEM_TYPE OS_BOOT_LOADER OS_ADD_BOOT_ARGS
}

job_setup() {
    if [ -v OS_BOOT_PART_SIZE ]; then
        citbx_export OS_BOOT_PART_SIZE
    fi
    TARGET_DOCKER_BUILD_FORCE=true
    ROOTFS_ARCHIVE_COMPRESS_DISABLED=true
    citbx_export TARGET_DOCKER_BUILD_FORCE ROOTFS_ARCHIVE_COMPRESS_DISABLED TARGET_MKDISK_ENABLED
}

job_main() {
    if [ -n "$CI_JOB_TOKEN" ] && [ "$DOCKER_HOST" != "unix:///var/run/docker.sock" ]; then
        # Login only if runned in DIND mode (e.g. on a Gitlab runner and not on a workstation)
        docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    fi
    if [ "$TARGET_DOCKER_BUILD_FORCE" != "true" ]; then
        # Trying to pull the image if exists
        if docker pull "$TARGET_CI_REGISTRY_IMAGE_TAG"; then
            TARGET_DOCKER_ALREADY_BUILDED=true
            print_note "Target image already builded, skipping build process..."
        fi
    fi
    if [ "$TARGET_DOCKER_ALREADY_BUILDED" != "true" ]; then
        target_build
    fi
    target_mkimage
    if [ "$OS_DISK_BUILD_ENABLED" == "true" ]; then
        target_mksystempart
        target_mksystemdisk
    elif [ "$OS_BOOT_PART_BUILD_ENABLED" == "true" ]; then
        target_mksystempart
    fi
}

job_after() {
    local retcode=$1
    if [ "$TARGET_DOCKER_ALREADY_BUILDED" != "true" ]; then
        if [ $retcode -eq 0 ]; then
            print_info "Image \"$TARGET_CI_REGISTRY_IMAGE_TAG\" successfully generated"
        fi
    fi
    docker rm -f $OS_DOCKER_EXPORT_ID || true
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$OS_IMAGE_DIR\" successfully generated"
        sudo rm -rf build
    fi
}
