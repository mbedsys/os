#!/bin/bash -e

set -e

_logger() {
    logger -t "system-update-script" -- "$@"
}

install_file() {
    local file_src=$1
    local file_dst=${3:-$file_src}
    local dest_dir=$2
    test -f "$file_src" && test -d "$dest_dir" || return 0
    if [ -f "$dest_dir/$file_dst" ]; then
        _logger "Backup existing file: $dest_dir/$file_dst => $dest_dir/$file_dst.old"
        mv $dest_dir/$file_dst{,.old}
    fi
    _logger "Installing new file [$dest_dir/$file_dst]..."
    cp "$file_src" "$dest_dir/$file_dst"
}

install_files() {
    local target_dir="/media/${1,,}"
    local target_dev="/dev/disk/by-label/${1^^}"
    test -e "$target_dev" || return 0
    if [ ! -d "$target_dir" ]; then
        mkdir -p "$target_dir"
        mount "$target_dev" "$target_dir"
    else
        mount -o remount,rw "$target_dir"
    fi
    local efi_boot_dir="$target_dir/$(cd "$target_dir" && find -ipath './efi/boot' | cut -c 3-)"
    # Grub part
    install_file grubx64.efi        "$efi_boot_dir"/            bootx64.efi
    install_file grub.cfg           "$target_dir"/boot/grub/
    install_file initramfs          "$target_dir"/boot/
    install_file vmlinuz            "$target_dir"/boot/
    # Full EFI part
    install_file linuxx64.efi       "$efi_boot_dir"/            bootx64.efi
    # Common part
    install_file rootfs.squashfs    "$target_dir"/
    mount -o remount,ro "$target_dir"
}

install_files system
install_files rescue

sync
_logger "System upgrade done!"
