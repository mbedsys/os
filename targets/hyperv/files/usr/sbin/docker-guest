#!/bin/bash -e

_logger() {
    case "$LOGGER_TYPE" in
        logger)
            logger -t "docker-guest" -- "$@"
        ;;
        stdout)
            echo "$@"
        ;;
    esac
}

if [ -z "$DOCKE_GUEST_DIR" ]; then
    if [ -d /media/docker-guests ]; then
        DOCKE_GUEST_DIR=/media/docker-guests
    else
        DOCKE_GUEST_DIR=/media/data/docker-guests
    fi
fi

VERSION=$(gawk '
    match($0, /^version: \s*['\''"]([0-9\.]+)['\''"]\s*$/, f) {
        print f[1];
    }' $DOCKE_GUEST_DIR/docker-compose.yml)
DOCKER_COMPOSE_OPTS=(
    --compatibility
    --project-name services
    --project-directory $DOCKE_GUEST_DIR
    -f $DOCKE_GUEST_DIR/docker-compose.yml
)

docker_compose_files_ckeck() {
    for f in $(ls $DOCKE_GUEST_DIR/docker-compose.*.yml 2>/dev/null || true); do
        if [ -f $f.sha512 ] \
            && sha512sum -c $f.sha512 > /dev/null 2>&1; then
            DOCKER_COMPOSE_OPTS+=(-f $f)
            continue
        fi
        _logger "Checking file: $f"
        sed -i '/^version:/{h;s/:.*/: "'"$VERSION"'"/};${x;/^$/{s//version: "'"$VERSION"'"/;H};x}' $f
        if docker-compose "${DOCKER_COMPOSE_OPTS[@]}" -f $f config >/dev/null 2> $f.log; then
            _logger "File [$f] check done."
            rm -f $f.log
        else
            _logger "File [$f] check fail!"
            cat $f.log | while read -r line; do
                _logger "Docker-compose error: $line"
            done
            mv $f $f.fail
            continue
        fi
        sha512sum $f > $f.sha512
        DOCKER_COMPOSE_OPTS+=(-f $f)
    done
}

docker_compose_files_ckeck
if [ "$DOCKER_GUEST_CHECK_CONFIG" == "true" ]; then
    if ! docker-compose "${DOCKER_COMPOSE_OPTS[@]}" config > /dev/null 2>&1; then
        rm -f $DOCKE_GUEST_DIR/docker-compose.*.yml.sha512
        docker_compose_files_ckeck
    fi
fi

_logger "Running:" docker-compose "${DOCKER_COMPOSE_OPTS[@]}" "$@"
docker-compose "${DOCKER_COMPOSE_OPTS[@]}" "$@"
