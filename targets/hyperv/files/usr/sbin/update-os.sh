#!/bin/bash -e

if [ $# -ne 1 ]; then
    echo "Usage: $0 <version>"
    exit 1
fi
eval "$(grep OS_NAME /etc/os-release*)"
URL_BASE=http://repo.mbedsys.org/system-images/$OS_NAME/$1
for f in $(wget -q $URL_BASE/upgrade.sha512 -O - | awk '{print $2;}') upgrade.sha512; do
    wget -c -P /media/data/upgrade/ $URL_BASE/$f
done
